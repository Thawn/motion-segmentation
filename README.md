# Motion Segmentation

Deep Neural Network that can segment objects based on their motion pattern.

Based on [this code](https://gitlab.com/wdeback/dl-conv2dlstm/blob/master/notebooks/notebook.ipynb) from [Walter de Back](https://gitlab.com/wdeback)

Check out the [example notebook](jupyter/Motion_Segmentation_Training_Prediction.ipynb) for how to train a network and use it for prediction.

## Getting started

### Create an environment

```bash
mamba create -n tf-motion-seg python=3.9 matplotlib numpy tifffile numexpr tqdm scikit-learn jupyter ffmpeg pydot
mamba activate tf-motion-seg
pip install tensorflow
```

### Clone the repository

```bash
git clone https://gitlab.com/Thawn/motion-segmentation.git
cd motion-segmentation
```

### Run the jupyter notebook for training a model

```bash
jupyter-lab jupyter/Motion_Segmentation_Training_Prediction.ipynb
```

You can also use the notebook for prediction. However, for predicting multiple files, the command line tool may be more convenient (see below).

## Command line prediction

A trained model can be applied to a folder of images using [`motion_segmentation.py`](src/motion_segmentation.py) from the command line:

```text
usage: motion_segmentation.py [-h] [-d PATH] [-t] [-s SIZE SIZE] [-p PADDING PADDING] [-l LENGTH] [-w WEIGHTS] [-g]

optional arguments:
  -h, --help            show this help message and exit
  -d PATH, --path PATH, --data-path PATH
                        path to your data.  Default is "../data/"
  -t, --tiles, --split-into-tiles
                        split the input stack into tiles.
  -s SIZE SIZE, --size SIZE SIZE, --tile-size SIZE SIZE
                        size of tiles into which the stack should be split. Default is 64x64 pixels. 
                        Only has an effect together with the --split-into-tiles flag.
  -p PADDING PADDING, --padding PADDING PADDING, --tile-padding PADDING PADDING
                        number of pixels that each tile should be padded in order to prevent edge artefacts. 
                        Default is 6x6 pixels. Only has an effect together with the --split-into-tiles flag.
  -l LENGTH, --length LENGTH, --len-sequences LENGTH
                        for memory reasons, the stack must be split into sequences. 
                        This parameter controls the number of frames per sequence.
  -w WEIGHTS, --weights WEIGHTS, --weights-file WEIGHTS
                        file name of the weights file. The file must be in the data path. 
                        Default is "training_weights.h5"
  -g, --gpu, --use-gpu  if this flag is set, use of GPU will be enforced. Make sure the GPU has enough memory 
                        to handle your data. For stacks that are larger than 128x128 pixels, it is 
                        recommended to use this only together with the --split-into-tiles flag.
```

For example:

```bash
python3 motion_segmentation.py -gts 128 128 --data-path "../data/"
```

An example [SLURM script](src/motion_segmentation.slurm) for execution on a high performanc computing cluster is also provided. It can be used with the same parameters as the python script. For example:

```bash
sbatch motion_segmentation.slurm -gts 128 128 --data-path "../data/"
```
