'''
motion_segmentation.py applies the motion segmentation model trained separately (in the jupyter notebook) to data supplied via command line
'''
import numpy as np
import os, glob
import tifffile
import time

#Configuration
data_path ='../data/'
tile_size = (64, 64)
tile_overlap = (8, 8) # how many pixels to add to each tile at the top/bottom, left/right should be equal to the number of convolutional layers
temporal_overlap = 2 #number of frames that each temporal tile overlaps.
len_sequences = 10 # number of frames in each image sequence that is evaluated together
weights_file = os.path.join(data_path, 'training_weights.h5')
use_gpu = False
split_into_tiles = False

if __name__ == '__main__':
    import argparse
    ap = argparse.ArgumentParser()
    ap.add_argument('-d', '--path', '--data-path', default=data_path, help='path to your data. Default is "../data/"')
    ap.add_argument('-t', '--tiles', '--split-into-tiles', action='store_true', help='split the input stack into tiles.')
    ap.add_argument('-s', '--size', '--tile-size', nargs=2, default=[64, 64], type=int, help='size of tiles into which the stack should be split. Default is 64x64 pixels. Only has an effect together with the --split-into-tiles flag.')
    ap.add_argument('-p', '--padding', '--tile-padding', nargs=2, default=[8, 8], type=int, help='number of pixels that each tile should be padded in order to prevent edge artefacts. Default is 6x6 pixels. Only has an effect together with the --split-into-tiles flag.')
    ap.add_argument('-l', '--length', '--len-sequences', default=len_sequences, type=int, help='for memory reasons, the stack must be split into sequences. This parameter controls the number of frames per sequence.')
    ap.add_argument('-w', '--weights', '--weights-file', default=os.path.basename(weights_file), help='file name of the weights file. The file must be in the data path. Default is "training_weights.h5"')
    ap.add_argument('-g', '--gpu', '--use-gpu', action='store_true', help='if this flag is set, use of GPU will be enforced. Make sure the GPU has enough memory to handle your data. For stacks that are larger than 128x128 pixels, it is recommended to use this only together with the --split-into-tiles flag.')
    args = ap.parse_args()
    data_path = args.path
    tile_size = (args.size[0], args.size[1])
    len_sequences = args.length
    weights_file = os.path.join(data_path, args.weights)
    use_gpu = args.gpu
    split_into_tiles = args.tiles
    print('tile size: {}, tile_overlap: {}, sequence length: {}, temporal padding: {}, weights file: {}, use GPU: {}, split into tiles: {}'.format(tile_size, tile_overlap, len_sequences, temporal_overlap, weights_file, use_gpu, split_into_tiles))


#Utility functions

# from https://github.com/CSBDeep/CSBDeep/blob/master/csbdeep/utils/utils.py
def normalize(x, pmin=1, pmax=99.8, axis=None, clip=False, eps=1e-20, dtype=np.float32):
    """
    Normalize an array `x` by scaling its values to lie within the range defined by the `pmin` and `pmax` percentiles.

    Parameters:
    -----------
    x : array_like
        The input array to be normalized.
    pmin : float, optional
        The lower percentile used to define the minimum value of the normalization range. Default is 1.
    pmax : float, optional
        The upper percentile used to define the maximum value of the normalization range. Default is 99.8.
    axis : int or tuple of ints, optional
        Axis or axes along which to compute the percentiles. Default is None.
    clip : bool, optional
        If True, clip the values outside the range defined by `pmin` and `pmax`. Default is False.
    eps : float, optional
        A small value to avoid division by zero. Default is 1e-20.
    dtype : data-type, optional
        The data type of the output array. Default is np.float32.

    Returns:
    --------
    out : ndarray
        The normalized array.
    """
    mi = np.percentile(x,pmin,axis=axis,keepdims=True)
    ma = np.percentile(x,pmax,axis=axis,keepdims=True)
    return normalize_mi_ma(x, mi, ma, clip=clip, eps=eps, dtype=dtype)

# from https://github.com/CSBDeep/CSBDeep/blob/master/csbdeep/utils/utils.py
def normalize_mi_ma(x, mi, ma, clip=False, eps=1e-20, dtype=np.float32):
    """
    Normalize the input array `x` using the minimum and maximum values `mi` and `ma`.

    Args:
        x (numpy.ndarray): Input array to be normalized.
        mi (float or numpy.ndarray): Minimum value(s) to be used for normalization.
        ma (float or numpy.ndarray): Maximum value(s) to be used for normalization.
        clip (bool, optional): If True, clip the output values to the range [0, 1]. Defaults to False.
        eps (float, optional): A small value to avoid division by zero. Defaults to 1e-20.
        dtype (numpy.dtype, optional): Data type to be used for computation. Defaults to np.float32.

    Returns:
        numpy.ndarray: Normalized array.

    """
    if dtype is not None:
        x   = x.astype(dtype,copy=False)
        mi  = dtype(mi) if np.isscalar(mi) else mi.astype(dtype,copy=False)
        ma  = dtype(ma) if np.isscalar(ma) else ma.astype(dtype,copy=False)
        eps = dtype(eps)

    try:
        import numexpr
        x = numexpr.evaluate("(x - mi) / ( ma - mi + eps )")
    except ImportError:
        x =                   (x - mi) / ( ma - mi + eps )

    if clip:
        x = np.clip(x,0,1)

    return x

def tile_images(x, num_tiles=4, tile_overlap=(8, 8)):
    """
    Tiles an input image into a grid of smaller images with overlap.

    Args:
        x (ndarray): The input image to tile.
        num_tiles (int): The number of tiles to create. Must be a perfect square.
        tile_overlap (tuple): The amount of overlap between tiles in pixels in x and y, respectively.

    Returns:
        ndarray: The tiled image grid.
    """
    num_rows = np.sqrt(num_tiles).astype('uint32')
    assert (num_tiles % num_rows) == 0, 'image cannot be split into %r tiles.' % num_tiles
    num_cols = num_tiles // num_rows
    shape = x.shape
    new_hw = np.divide(shape[1:3],[num_rows, num_cols]).astype('uint32')
    output = np.zeros((shape[0] * num_tiles, new_hw[0] + 2 * tile_overlap[0], new_hw[1] + 2 * tile_overlap[1]) + shape[3:])
    x = np.pad(x,((0, 0), (tile_overlap[0], tile_overlap[0]), (tile_overlap[1], tile_overlap[1]), (0, 0)), 'constant')
    print('original shape: {}, new_hw: {}, x_shape: {}'.format(shape, new_hw, x.shape))
    tile = 1
    for r in range(num_rows):
        h_start = r * new_hw[0]
        h_end = (r + 1) * new_hw[0] + 2 * tile_overlap[0]
        for c in range(num_cols):
            output[(tile - 1) * shape[0]:tile * shape[0],:,:,:] = x[:, h_start:h_end, c * new_hw[1]:(c + 1) * new_hw[1] + 2 * tile_overlap[1],:]
            tile += 1
    return output

def assemble_tiled_images(x, num_tiles=4, tile_overlap=(8, 8)):
    """
    Assemble tiled images into a single image.

    Args:
        x (numpy.ndarray): Input array of tiled images.
        num_tiles (int): Number of tiles to assemble.
        tile_overlap (tuple): Overlap between tiles.

    Returns:
        numpy.ndarray: Assembled image.
    """
    num_rows = np.sqrt(num_tiles).astype('uint32')
    assert (num_tiles % num_rows) == 0, 'image cannot be assembled from %r tiles.' % num_tiles
    num_cols = num_tiles // num_rows
    shape = x.shape
    shape = (shape[0], shape[1] - 2 * tile_overlap[0], shape[2] - 2 * tile_overlap[1]) + shape[3:]
    num_tiles = shape[0] // num_tiles
    new_hw = np.prod([shape[1:3],[num_rows, num_cols]],axis=0).astype('uint32')
    output = np.zeros((num_tiles,new_hw[0],new_hw[1])+shape[3:])
    tile = 1
    start_x, start_y = tile_overlap
    end_x = tile_overlap[0] + shape[1]
    end_y = tile_overlap[1] + shape[2]
    for r in range(num_rows):
        for c in range(num_cols):
            output[:,r * shape[1]:(r + 1) * shape[1],c * shape[2]:(c + 1) * shape[2],:] = x[(tile - 1) * num_tiles:tile * num_tiles, start_x:end_x, start_y:end_y, :]
            tile += 1
    return output

import numpy as np

def slice_stack(x, len_sequences=10, temporal_overlap=2):
    """
    Slices the input array into smaller sequences with overlap.

    Args:
        x (numpy.ndarray): Input array to be sliced.
        len_sequences (int): Length of the sequences to be sliced. Default is 10.
        temporal_overlap (int): Number of frames to be used as overlap between sezuences. Default is 2. The first frame is padded with the respective number of frames containing zeros.

    Returns:
        numpy.ndarray: Sliced and padded array.

    """
    num_sequences = x.shape[0] // len_sequences
    x = np.array(np.split(x, np.ceil(num_sequences)))
    if temporal_overlap > 0:
        output_stack = np.zeros((num_sequences, len_sequences + temporal_overlap) + x.shape[2:])
        for s in range((np.ceil(num_sequences)).astype('uint32')):
            if s == 0:
                output_stack[s] = np.insert(x[s], 0,x[s,0:temporal_overlap], axis=0)
            else:
                output_stack[s] = np.insert(x[s], 0,x[s-1,-temporal_overlap:], axis=0)
        x = output_stack
    print('sliced stack shape: {}, min: {}, max: {}'.format(x.shape, x.min(), x.max()))
    return x

import numpy as np

def concatenate_slices(x, temporal_overlap=2):
    """
    Concatenates the slices of a 3D numpy along the first axis while removing `temporal_overlap` slices.

    Used to re-assemble a stack of images that was sliced into sequences of images by slice_stack().

    Args:
        x (numpy.ndarray): The input 3D numpy array.
        temporal_overlap (int): The number of slices to remove from the beginning of each sequence.

    Returns:
        numpy.ndarray: The concatenated 3D numpy array.
    """
    output = np.zeros((x.shape[0], x.shape[1] - temporal_overlap) + x.shape[2:])
    for s in range(x.shape[0]):
        output[s,:] = x[s, temporal_overlap:]
    return np.concatenate(output, axis=0)

def load_stack(stack_name, data_path='../data/', len_sequences=10, dtype=np.float32, norm=True):
    """
    Load a stack of images from a TIFF file and preprocess it.

    Args:
        stack_name (str): Name of the TIFF file containing the stack.
        data_path (str, optional): Path to the directory containing the TIFF file. Defaults to '../data/'.
        len_sequences (int, optional): Length of the image sequences in the stack. Defaults to 10.
        dtype (numpy.dtype, optional): Data type to which the stack should be converted. Defaults to np.float32.
        norm (bool, optional): Whether to normalize the stack. Defaults to True.

    Returns:
        numpy.ndarray: The loaded and preprocessed stack of images.
    """
    stack = tifffile.imread(os.path.join(data_path, stack_name))
    if stack.shape[0] % len_sequences > 0:
        stack = stack[:stack.shape[0]//len_sequences * len_sequences,:,:]
    
    #convert stack data type
    stack = stack.astype(dtype)
    
    if norm:
        #normalize stack
        stack = normalize(stack, pmax=99.95, clip=False)

    print('stack shape: {}, min: {}, max: {}'.format(stack.shape, stack.min(), stack.max()))
    return stack

def format_stack(stack, tile_size=(64, 64), tile_overlap=(8, 8), temporal_overlap=2, len_sequences=10):
    """
    Formats a stack of images for use in motion segmentation.

    Args:
        stack (numpy.ndarray): A stack of images.
        tile_size (tuple, optional): The size of each tile. Defaults to (64, 64).
        tile_overlap (tuple, optional): The padding between each tile. Defaults to (8, 8).
        temporal_overlap (int, optional): The padding between each sequence of tiles. Defaults to 2.
        len_sequences (int, optional): The length of each sequence of tiles. Defaults to 10.

    Returns:
        numpy.ndarray: The formatted stack of images.
        int: The number of tiles in the stack.
    """
    #add slices and color channel axes
    formatted_stack = stack[..., np.newaxis]
    
    num_tiles = np.prod(np.divide(np.array(stack.shape[1:3]), np.array(tile_size)))
    num_tiles = num_tiles.astype('uint32')
    
    formatted_stack = tile_images(formatted_stack, num_tiles, tile_overlap=tile_overlap)
    
    formatted_stack = slice_stack(formatted_stack, len_sequences, temporal_overlap=temporal_overlap)
    
    print('formatted stack shape: {}, min: {}, max: {}'.format(formatted_stack.shape, formatted_stack.min(), formatted_stack.max()))
    return formatted_stack, num_tiles

if not use_gpu:
    os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"   # see issue #152
    os.environ["CUDA_VISIBLE_DEVICES"] = ""
from keras.layers import (Input, Conv2D, ConvLSTM2D, Conv3D, Concatenate, BatchNormalization, TimeDistributed) 
from keras.models import Model
from keras.callbacks import ModelCheckpoint

def build_model_dense(input_shape, len_embedding):
    """
    Builds a dense model for motion segmentation.

    Args:
    input_shape (tuple): The shape of the input data.
    len_embedding (int): The length of the embedding (i.e. the number of different classes to segment).

    Returns:
    A Keras model for motion segmentation.
    """
    i = Input(input_shape)
    
    #use Conv3D instead of TimeDistributed because TimeDistributed does not allow variable size data
    x1 = Conv3D(32, (1,3,3), activation='relu', padding='same')(i)
    x1 = BatchNormalization()(x1)
    
    x2 = Conv3D(32, (1,3,3), activation='relu', padding='same')(x1) 
    x2 = BatchNormalization()(x2)
    
    x = Concatenate()([x1,x2])

    x3 = ConvLSTM2D(32, kernel_size=(3,3), padding='same', return_sequences=True)(x)
    x3 = BatchNormalization()(x3)
    
    x = Concatenate()([x1,x2,x3])
    
    x4 = ConvLSTM2D(32, kernel_size=(3,3), padding='same', return_sequences=True)(x)
    x4 = BatchNormalization()(x4)
    
    x = Concatenate()([x1,x2,x3,x4])
    
    x5 = Conv3D(32, (1,3,3), activation='relu', padding='same')(x) 
    x5 = BatchNormalization()(x4)
    
    x = Concatenate()([x1,x2,x3,x4,x5])
    
    x6 = ConvLSTM2D(32, kernel_size=(3,3), padding='same', return_sequences=True)(x)
    x6 = BatchNormalization()(x6)
    
    x = Concatenate()([x1,x2,x3,x4,x5,x6])
    
    o = Conv3D(len_embedding, kernel_size=(3,3,3), activation='sigmoid', padding='same')(x)
    return Model(inputs=i, outputs=o)

#Dice coefficient loss function
def dice_coefficient(y_true, y_pred):
    """
    A statistic used for comparing the similarity of two samples. Here binary segmentations.

    Args:
        y_true (numpy.array): the true segmentation
        y_pred (numpy.array): the predicted segmentation

    Returns:
        (float) returns a number from 0. to 1. measuring the similarity y_true and y_pred
    """
    from keras import backend as K
    y_true_f=K.flatten(y_true)
    y_pred_f=K.flatten(y_pred)
    intersection=K.sum(y_true_f*y_pred_f)
    smooth=1.0
    return (2*intersection+smooth)/(K.sum(y_true_f)+K.sum(y_pred_f)+smooth)
    
def dice_loss(y_true, y_pred):
    """
    Calculates the Dice loss between the true and predicted segmentation masks.

    Args:
        y_true (tensor): The ground truth segmentation mask.
        y_pred (tensor): The predicted segmentation mask.

    Returns:
        The Dice loss between the true and predicted segmentation masks.
    """
    return 1-dice_coefficient(y_true, y_pred)

# Build model
model = build_model_dense(input_shape=(None, None, None, 1), len_embedding=1)
model.compile(loss=dice_loss, optimizer='adam')# metrics=[dice_coefficient])

# Load weights
print('loading weights: ' + weights_file)
os.environ["HDF5_USE_FILE_LOCKING"] = 'FALSE' #needed to prevent error on lustre filesystem
model.load_weights(weights_file)

# Process tiff files in data_path
tiff_files = glob.glob(os.path.join(data_path, '*.tif'))
tiff_files += glob.glob(os.path.join(data_path, 'node*/*.tif'))
for f in tiff_files:
    start_time = time.time()
    print('-----------------------')
    print('processing stack: ' + f)
    stack = load_stack(f, data_path='', len_sequences=len_sequences)
    if split_into_tiles:
        stack, num_tiles = format_stack(stack, tile_size, tile_overlap, temporal_overlap, len_sequences)
        stack_image_size = stack.shape[1] * stack.shape[2]
        if (stack_image_size > 128 * 128) and use_gpu:
            import warnings
            warnings.warn("your tiles are larger than 128x128 pixels and you are trying to run the prediction on a GPU. If the prediction fails with an out of memory error, try to run again without the --use-gpu flag or use a smaller tile size.")
    else:
        stack = slice_stack(stack, len_sequences, temporal_overlap)
        stack = stack[..., np.newaxis]
        stack_image_size = stack.shape[1] * stack.shape[2]
        if (stack_image_size > 128 * 128) and use_gpu:
            import warnings
            warnings.warn("your images are larger than 128x128 pixels and you are trying to run the prediction on a GPU. If the prediction fails with an out of memory error, try to run again without the --use-gpu flag or use the --split-into-tiles flag (and the --tile-size parameter) to split your image into tiles.")
    predicted_stack = model.predict(stack, batch_size=1)
    predicted_stack = concatenate_slices(predicted_stack, temporal_overlap)
    if split_into_tiles:
        predicted_stack = assemble_tiled_images(predicted_stack, num_tiles, tile_overlap)
    path = os.path.join(os.path.dirname(f),'eval')
    os.makedirs(path, exist_ok=True)
    threshold_file = os.path.join(path, os.path.basename(f) + '_Threshold.tif')
    print('saving threshold: ' + threshold_file)
    tifffile.imsave(threshold_file, (predicted_stack * 255).astype(np.uint8), compress=5)
    print("--- elapsed time: %s seconds ---" % (time.time() - start_time))